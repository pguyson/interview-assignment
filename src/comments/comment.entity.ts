import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('comments')
export class CommentEntity {
  @PrimaryGeneratedColumn('increment')
  id: number

  @Column({
    type: 'text',
  })
  description: string

  @Column({
    type: 'timestamp',
    default: 'now()',
  })
  createdAt: string
}
