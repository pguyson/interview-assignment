import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('posts')
export class PostEntity {
  @PrimaryGeneratedColumn('increment')
  id: number

  @Column({
    length: '250',
  })
  title: string

  @Column({
    type: 'text',
  })
  description: string

  @Column({
    type: 'timestamp',
    default: 'now()',
  })
  createdAt: string
}
