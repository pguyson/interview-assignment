import { Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  const config: ConfigService = app.get(ConfigService)
  const port = config.get<number>('PORT') || 3000
  const environment = config.get<string>('NODE_ENV') || 'development'
  await app.listen(port)
  Logger.log(
    `Server ${environment} running on http://localhost:${port}`,
    'Bootstrap',
  )
}
bootstrap()
