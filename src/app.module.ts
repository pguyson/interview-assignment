import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import * as fs from 'fs'
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm'
import { PostsModule } from './posts/posts.module'
import { AuthModule } from './auth/auth.module'
import { CommentsModule } from './comments/comments.module'
@Module({
  imports: [
    fs.existsSync('local.env')
      ? ConfigModule.forRoot({
          envFilePath: `${process.env.NODE_ENV || 'local'}.env`,
          isGlobal: true,
        })
      : ConfigModule.forRoot({
          isGlobal: true,
        }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (
        configService: ConfigService,
      ): Promise<TypeOrmModuleOptions> => {
        return {
          type: 'postgres',
          host: configService.get<string>('DB_HOST'),
          port: configService.get<number>('DB_PORT'),
          username: configService.get<string>('DB_USERNAME'),
          password: configService.get<string>('DB_PASSWORD'),
          database: configService.get<string>('DB_NAME'),
          entities: [__dirname + '/**/*.entity{.ts,.js}'],
          synchronize: true,
          logging: true,
        }
      },
      inject: [ConfigService],
    }),
    PostsModule,
    AuthModule,
    CommentsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
