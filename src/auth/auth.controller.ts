import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { SignInDto } from './sign-in.dto'

@Controller('auth')
export class AuthController {
  @UseGuards(AuthGuard('local'))
  @Post('signin')
  async signIn(@Req() req, @Body() signInDto: SignInDto) {
    return req.user
  }
}
