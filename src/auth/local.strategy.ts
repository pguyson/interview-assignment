import { Injectable, UnauthorizedException } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { Strategy } from 'passport-local'
import { AuthService } from './auth.service'
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private AuthService: AuthService) {
    super()
  }

  async validate(username: string, password: string): Promise<any> {
    const user = this.AuthService.validate(username, password)
    if (!user) {
      throw new UnauthorizedException()
    }
    return user
  }
}
