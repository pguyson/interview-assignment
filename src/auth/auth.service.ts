import { Injectable } from '@nestjs/common'

@Injectable()
export class AuthService {
  private users = [
    {
      id: 1,
      username: 'user',
      password: 'user1',
    },
  ]

  validate(username: string, password: string) {
    const user = this.users.find(u => u.username === username)
    if (!user) {
      return false
    }
    if (user.password !== password) return false
    delete user.password
    return user
  }
}
